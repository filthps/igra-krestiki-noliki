## Блок-схемы ##

Игра разделена на 3 класса в соотвествии со схемой разделения данных - (Model-View-Controller).
Рассмотрим блок-схемы в следующий последовательности, включая методы классов в виде подпунктов:

* Core - Логика приложения (1)
* Cli - Интерфейс командной строки (2)
* Game - Контроллер, обеспечивающий взамодействие представления (Cli) и логики (Core). Использует экземпляры (Core и Cli), без наследования (3)

1

![Core](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/cd65235d7c959aa64dfee6a78ba7ce84b74dad1b/schemes/logic/class_Core.png)

--------

1.1

![drop_lines](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/99fb476d64c89587a75671c76c7af147cb4fe873/schemes/logic/drop_lines.png)

--------

1.2

![find_winner](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/cd65235d7c959aa64dfee6a78ba7ce84b74dad1b/schemes/logic/find_winner.png)

--------

1.3

![init_logic](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/cd65235d7c959aa64dfee6a78ba7ce84b74dad1b/schemes/logic/init_logic.png) 

--------

1.5

![move](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/cd65235d7c959aa64dfee6a78ba7ce84b74dad1b/schemes/logic/move.png)

--------

1.6

![update_logic](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/cd65235d7c959aa64dfee6a78ba7ce84b74dad1b/schemes/logic/update_logic.png)

--------
--------

2

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/class_Cli.png)

--------

2.1

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/init_view_field.png)

--------

2.2

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/end_game.png)

--------

2.3

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/input.png)

--------

2.4

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/move.png)

--------

2.5

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/output.png)

--------

2.6

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/show_view.png)

--------

2.7

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/cli/update_view.png)

--------
--------

3

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/Game/class_Game.png)

--------

3.1

![Cli](https://bitbucket.org/filthps/igra-krestiki-noliki/raw/f0ea3c070ae8182288f48c057115b64ab536b412/schemes/Game/move.png)