from __future__ import annotations  # Python < 3.7
import sys
from typing import Union, Callable, Optional
from collections import OrderedDict as Ord_dict


SIZE = 3
EMPTY_RECT_SYMBOL = "-"


class Cli:

    def __init__(self, size: int = SIZE, empty_symbol: str = EMPTY_RECT_SYMBOL):
        self.SIZE: int = size
        self.EMPTY_SYMBOL: str = empty_symbol
        self.view_field: dict = {}

    def output(self, view: dict[str, str]) -> str:
        size = self.SIZE - 1
        output_string = ""
        counter = 0
        for val in view.values():
            output_string += val
            if counter == size:
                output_string += "\r\n"
                counter = 0
            else:
                counter += 1
        return output_string

    def init_view_field(self):
        if float(sys.version[:3]) < 3.9:
            view = Ord_dict()
        else:
            view = {}
        empty = self.EMPTY_SYMBOL
        size = self.SIZE - 1
        counter_1, counter_2 = 0, size
        for i in range((size + 1) ** 2):
            view.update({f"{counter_1}{counter_2}": empty})
            if counter_1 == size:
                counter_1, counter_2 = 0, counter_2 - 1
            else:
                counter_1 += 1
        return view

    def input(self, view: dict[str, str], walker: str) -> str:
        coordinates = input("Сейчас вы ходите за - {0}, введите координаты хода:".format(walker))
        if coordinates.isdigit() and len(coordinates) == 2:
            coordinates_string = "".join([str(int(i) - 1) for i in coordinates])
            value = view.get(coordinates_string, False)
            if value:
                if value == self.EMPTY_SYMBOL:
                    return coordinates_string
                else:
                    self.input(view, walker)
        else:
            method: Union[bool, Callable] = getattr(self, coordinates, False)
            if method:
                return method()
        return self.input(view, walker)

    def update_view(self, coordinate: str, value: str) -> dict[str, str]:
        view = self.view_field.copy()
        view[coordinate] = value
        return view

    def show_view(self):
        print(self.output(self.view_field))

    def end_game(self, status, walker):
        if status:
            print("Победил - {0}!".format(walker))
        elif status is None:
            print("Ничья!")

    def move(self, walker: str, coordinates: str):
        coordinates = self.input(self.view_field, walker)
        self.view_field = self.update_view(coordinates, walker)
        self.show_view()
        return coordinates

    def __call__(self):
        self.view_field: dict[str, str] = self.init_view_field()
