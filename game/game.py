from __future__ import annotations  # Python < 3.7
from cli import Cli
from game_logic import Core


SIZE = 3
FIRST = True  # True - "0", False - "X"
EMPTY_RECT_SYMBOL = "-"


class Game:

    def __init__(self, size: int, first_move: bool, empty_symbol: str):
        if not isinstance(size, int):
            raise TypeError
        if size >= 100:
            raise ValueError
        if not isinstance(first_move, bool):
            raise TypeError
        if not isinstance(empty_symbol, str):
            raise TypeError
        if not empty_symbol:
            raise ValueError
        self.walker: bool = first_move
        self.size: int = size
        self.logic = Core(size)
        self.interface = Cli(size, empty_symbol)

    def move(self):
        walker = self.walker
        walker_symbol = ["Х", "0"][walker]
        coordinates = self.interface.move(walker_symbol)
        status = self.logic.move(coordinates, walker_symbol)
        if not status:
            self.walker = not walker
            return self.move()
        else:
            self.interface.end_game(status, walker_symbol)

    def __call__(self):
        self.logic()
        self.interface()
        self.move()


if __name__ == "__main__":
    game = Game(SIZE, FIRST, EMPTY_RECT_SYMBOL)
    game()
