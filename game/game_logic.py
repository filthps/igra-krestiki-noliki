from __future__ import annotations  # Python < 3.7
from typing import Optional, Union, OrderedDict
from copy import deepcopy


SIZE = 3


class Core:

    def __init__(self, size=SIZE):
        self.logic_field = {}
        self.SIZE = size

    def find_winner(self, field: Union[OrderedDict[str, dict[str, dict[str, set, int, set]]],
                                       dict[str, dict[str, dict[str, set, int, set]]]]) -> \
            tuple[Optional[bool], dict[str, dict[str, set, int, set]]]:
        size = self.SIZE
        remove = {}
        if not field:
            return None, remove
        for line_type, line in field.items():
            for key, position in line.items():
                items, counter, line_keys = position.values()
                if len(items) > 1:
                    remove.update({line_type: line_keys})
                else:
                    if counter == size:
                        return True, remove
        return False, remove

    def drop_lines(self, field: Union[OrderedDict[str, dict[str, dict[str, set, int, set]]],
                                      dict[str, dict[str, dict[str, set, int, set]]]],
                   remove: dict[str, dict[str, set, int, set]]) \
            -> Union[OrderedDict[str, dict[str, dict[str, set, int, set]]],
                     dict[str, dict[str, dict[str, set, int, set]]]]:
        field_copy = field.copy()
        for line_type, items_index in remove.items():
            lines = field_copy.get(line_type, False)
            for index in items_index:
                if lines:
                    del field_copy[line_type][index]
                    if not lines:
                        del field_copy[line_type]
        return field_copy

    def update_logic(self, coordinate: str, value: str) -> \
            Union[OrderedDict[dict[str: set, int, set]], dict[dict[str: set, int, set]]]:
        field_copy = self.logic_field.copy()
        for line_type, line in self.logic_field.items():
            if coordinate in line:
                position = field_copy[line_type][coordinate]
                position["uniqueness_control"].add(value)
                position["move_counter"] += 1
        return field_copy

    def init_logic_field(self):
        size = self.SIZE
        counter = size
        line_range = range(size)
        diagonal_1_keys, diagonal_2_keys = set(), set()
        x_lookup, y_lookup, xy_lookup, yx_lookup = {}, {}, {}, {}
        field = {
            "x": x_lookup,
            "xy": xy_lookup,
            "y": y_lookup,
            "yx": yx_lookup,
        }
        line = {"uniqueness_control": set(), "move_counter": 0, "line_items": None}
        for i in line_range:
            x_keys, y_keys = set(), set()
            for u in line_range:
                x_keys.add(f"{u}{i}")
                y_keys.add(f"{i}{u}")
            counter -= 1
            diagonal_2_keys.add(f"{i}{counter}")
            if not counter:
                counter = size
            x_line, y_line = deepcopy(line), deepcopy(line)
            x_line["line_items"], y_line["line_items"] = x_keys, y_keys
            d1: dict = {}
            d2: dict = {}
            d1 = d1.fromkeys(x_keys, x_line)
            d2 = d2.fromkeys(y_keys, y_line)
            x_lookup.update(d1)
            y_lookup.update(d2)
            diagonal_1_keys.add(f"{i}{i}")
        xy_line, yx_line = deepcopy(line), deepcopy(line)
        xy_line["line_items"], yx_line["line_items"] = diagonal_1_keys, diagonal_2_keys
        d1, d2 = {}, {}
        d1 = d1.fromkeys(diagonal_1_keys, xy_line)
        d2 = d2.fromkeys(diagonal_2_keys, yx_line)
        xy_lookup.update(d1)
        yx_lookup.update(d2)
        return field

    def move(self, coordinates: str, walker: str):
        logic_field = self.update_logic(coordinates, walker)
        status, remove_fields_id = self.find_winner(logic_field)
        if remove_fields_id:
            logic_field = self.drop_lines(logic_field, remove_fields_id)
        self.logic_field = logic_field
        return status

    def __call__(self):
        self.logic_field = self.init_logic_field()
